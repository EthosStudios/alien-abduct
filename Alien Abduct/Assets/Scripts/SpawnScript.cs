﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnScript : MonoBehaviour {

	// Test variables
	public GameObject testPerson;

	// Prefabs
	public GameObject abducteePrefab; // The abductee prefab
	public int maxPeople; // Max number of people in the scene

	// Configuration settings
	public float minSpeed;
	public float maxSpeed;

	// Configuration parameters
	private Camera _gameCamera;
	private Vector3 _screenEdgeTR;
	private Vector3 _screenEdgeBL;
	private float _screenWidth;
	private float _screenHeight;

	// Spawn items
	private List<SpawnObject> people = new List<SpawnObject>();

	// Initialize any pre-game parameters
	void Awake() {
		minSpeed = minSpeed == 0 ? 0.5F : minSpeed;
		maxSpeed = maxSpeed == 0 ? 4F : maxSpeed;
	}

	// Use this for initialization
	void Start () {
		_gameCamera = Camera.main;

		_screenEdgeTR = _gameCamera.ViewportToWorldPoint (new Vector3 (1F, 1F, 1F)); // Top right screen edge
		_screenEdgeBL = _gameCamera.ViewportToWorldPoint (new Vector3 (0F, 0F, 0F)); // Bottom left screen edge
		_screenWidth = _screenEdgeTR.x - _screenEdgeBL.x;
		_screenHeight = _screenEdgeTR.y - _screenEdgeBL.y;

		StartCoroutine(SpawnObjects(abducteePrefab, maxPeople, people));

	}
	
	// Update is called once per frame
	void Update () {
		// The speed of each item is defined in it's SpawnObject variable "speed"
		foreach (SpawnObject person in people) {
			if (person.spawnObj.tag == "Default") continue; // Skip movement function for efficiency
			MoveItem(person);
		}


	}
		
	// Moves an item left or right on the screen based upon it's tag
	public void MoveItem(SpawnObject spawnItem) {
		GameObject item = spawnItem.spawnObj;
		float speed = spawnItem.speed * Time.deltaTime;
		Vector3 position = item.transform.position;

		switch (item.tag) {
			case "Right Spawn":
				if (position.x <= _screenEdgeBL.x - 0.5F) {
					CleanUpSpawnItem(spawnItem);
					break;
				}
				item.transform.Translate(-speed, 0, 0);
				break;
			case "Left Spawn":
				if (position.x >= _screenEdgeTR.x + 0.5F) {
					CleanUpSpawnItem(spawnItem);
					break;
				}
				item.transform.Translate(speed, 0, 0);
				break;
			default:
				break;
			}
	}

	// Reset spawn item parameters. Position off screen and set it's tag name to Default for no movement.
	public void CleanUpSpawnItem(SpawnObject item) {
		GameObject obj = item.spawnObj;
		obj.tag = "Untagged"; // Don't allow the object to be handled by the Update() function

		Rigidbody2D physics = obj.GetComponent<Rigidbody2D>();
		physics.isKinematic = true; // Don't let gravity affect this object

		Vector3 position = obj.transform.position;
		position.x = _screenEdgeTR.x + 2; // Place at an arbitrary location to prepare to be used again.

		obj.transform.position = position;

		int delay = Random.Range(1, 4);
		StartCoroutine(RestartObject(item, delay));
	}

	// Create new spawn GameObject and set it
	IEnumerator SpawnObjects(GameObject prefab, int numObjects, List<SpawnObject> spawnList) {

		for (int i = 0; i < numObjects; i++) {
			GameObject spawnItem = Instantiate(prefab);
			SpriteRenderer renderer = spawnItem.GetComponent<SpriteRenderer>();
			Vector3 position = spawnItem.transform.position;

			// Every other person goes on the right side of the screen
			if (i%2 == 0) {
				spawnItem.tag = "Right Spawn";
				position.x = _screenEdgeTR.x + 0.5F;
				renderer.flipX = false;
			
			} else {
				spawnItem.tag = "Left Spawn";
				position.x = _screenEdgeBL.x - 0.5F;
				renderer.flipX = true;
			}

			spawnItem.transform.position = position;


			float speed = Random.Range(minSpeed, maxSpeed);
			SpawnObject spawnObject = new SpawnObject(spawnItem, speed);
			spawnList.Add(spawnObject);

			yield return new WaitForSeconds(Random.Range(1, 3));
		}
	}

	IEnumerator RestartObject(SpawnObject obj, int delay) {
		yield return new WaitForSeconds(delay);

		GameObject gameObj = obj.spawnObj;
		Rigidbody2D physics = gameObj.GetComponent<Rigidbody2D>();
		SpriteRenderer renderer = gameObj.GetComponent<SpriteRenderer>();
		Vector3 position = gameObj.transform.position;
			
		if (Random.Range(0, 10) % 2 == 0) {
			gameObj.tag = "Right Spawn";
			position.x = _screenEdgeTR.x + 0.5F;
			renderer.flipX = false;

		} else {
			gameObj.tag = "Left Spawn";
			position.x = _screenEdgeBL.x - 0.5F;
			renderer.flipX = true;
		}
			
		obj.speed = Random.Range(minSpeed, maxSpeed);
		gameObj.transform.position = position;
		physics.isKinematic = false;
	}


}
