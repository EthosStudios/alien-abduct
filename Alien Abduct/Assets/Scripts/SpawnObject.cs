﻿using UnityEngine;
using System.Collections;

public class SpawnObject {

	public GameObject spawnObj; // The gameObject that will interact with the scene
	public float speed = 1F;

	public SpawnObject() {
		
	}

	public SpawnObject(GameObject obj) {
		spawnObj = obj;
	}

	public SpawnObject(GameObject obj, float speed) {
		spawnObj = obj;
		this.speed = speed;
	}

}
